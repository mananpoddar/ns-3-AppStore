#!/usr/bin/env bash
set -xv
#$1 = method either git or direct
#$2 = url
#$3 = ns-3 version
#$4 = version tag if method is git
if [ "${1}" != "git" ] && [ "${1}" != "direct" ]
then
    exit 1
fi
if [ "${1}" = "git" ] && [ $# -lt 4 ]
then
    echo Requires 4 args
    exit 1
fi
if [ "${1}" = "direct" ] && [ $# -lt 3 ]
then
    echo Requires 3 args
    exit 1
fi

#get bake
git clone https://gitlab.com/nsnam/bake.git || exit $?
cd bake || exit $?

#configure, download ns version
./bake.py configure -e $3 -m || exit $?
./bake.py --noColor download || exit $?
test -d source/$3 || exit $?

#get the module from git or direct
cd source/$3/contrib || exit $?
if [ "${1}" = "git" ]
then
    git clone -b $4 --single-branch $2 || exit $?
else
    wget -O module $2 || exit $?
    file_type=$(file --mime-type -b module) || exit $?
    if [ "$file_type" = "application/zip" ]
    then
        #.zip
        unzip module || exit $?
    elif [ "$file_type" = "application/gzip" ]
    then
        #.tar.gz
        tar -xzf module || exit $?
    elif [ "$file_type" = "application/x-bzip2" ]
    then
        #.tar.bz2
        tar -xjf module || exit $?
    elif [ "$file_type" = "application/x-tar" ]
    then
        #.tar
        tar -xf module || exit $?
    else
        echo "Unknown format used"
        exit 1
    fi
    rm module || exit $?
fi

#configure, build the ns-3 and run tests module and run tests
cd .. || exit $?
./waf -d debug configure --enable-examples --enable-tests --enable-mpi || exit $?
./waf || exit $?
./test.py || exit $?
./waf clean || exit $?
./waf -d optimized configure --enable-examples --enable-tests --enable-mpi || exit $?
./waf || exit $?
./test.py || exit $?
./waf clean || exit $?
./waf -d optimized configure --enable-static --enable-tests --enable-mpi || exit $?
./waf || exit $?
./test.py || exit $?
./waf clean || exit $?
./waf -d release configure --enable-examples --enable-tests --enable-mpi || exit $?
./waf || exit $?
./test.py || exit $?
./waf clean || exit $?